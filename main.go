package main

import (
	"app/handlers"
	"app/utils"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
)

func main() {
	config := handlers.Config{}
	config.Init("dev")

	e := echo.New()
	e.Validator = &utils.Validator{Validator: validator.New()}

	handlers.InitRoutes(e, config)
	e.Logger.Fatal(e.Start(":8080"))
}