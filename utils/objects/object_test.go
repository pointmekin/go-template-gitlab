package objects

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_GetObjectKeys(t *testing.T) {
	obj := map[string]interface{}{
		"key1": "value1",
		"key2": "value2",
	}

	keys := GetObjectKeys(obj)
	assert.Equal(t, len(keys), 2)
	assert.Equal(t, keys[0], "key1")
	assert.Equal(t, keys[1], "key2")
}

func Test_GetObjectValues(t *testing.T) {
	obj := map[string]interface{}{
		"key1": "value1",
		"key2": "value2",
	}

	values := GetObjectValues(obj)
	assert.Equal(t, []interface{}{"value1", "value2"}, values)
}