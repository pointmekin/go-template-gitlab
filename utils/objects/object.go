package objects

func GetObjectKeys(obj map[string]interface{}) []string {
	keys := make([]string, 0, len(obj))
	for k := range obj {
		keys = append(keys, k)
	}
	return keys
}

func GetObjectValues(obj map[string]interface{}) []interface{} {
	values := make([]interface{}, 0, len(obj))
	for _, v := range obj {
		values = append(values, v)
	}
	return values
}