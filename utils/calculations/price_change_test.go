package calculations

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_CalculatePriceChange(t *testing.T) {
	t.Run("Should calculate price change", func(t *testing.T) {
		result, err := CalculatePriceChange(100.50, 200.00, 100)
		assert.Equal(t, err, nil)

		if result != (200.00-100.50) * 100 {
			t.Errorf("Expected 10,000, got %v", result)
		}

		result, err = CalculatePriceChange(100, 200, 0)
		assert.Equal(t, err, nil)
		
		if result != (200-100) * 0 {
			t.Errorf("Expected 0, got %v", result)
		}
	})

	t.Run("Should return error", func(t *testing.T) {
		change, err := CalculatePriceChange(100, 200, -1)
		assert.EqualError(t, err, "volume cannot be negative")
		assert.Equal(t, change, float64(0))
	})
}

func Test_CalculatePriceChangePercent(t *testing.T) {
	t.Run("Should calculate price change percent", func(t *testing.T) {
		val := CalculatePriceChangePercent(100.50, 200.00)

		if val != 0.99005 	 {
			t.Errorf("Expected 0.99005, got %v", val)
		}
	})
}

func Test_CalculateRiskToReward(t *testing.T) {
	t.Run("Should calculate risk to reward", func(t *testing.T) {
		val := CalculateRiskToReward(100.50, 200.00, 95.00)

		if val != 18.09091 {
			t.Errorf("Expected 18.09091, got %v", val)
		}
	})
}

func Test_CalculateBalanceAfter(t *testing.T) {
	t.Run("Should calculate balance after with positive change", func(t *testing.T) {
		val := CalculateBalanceAfter(1_000_000, 10_000, 15)
		fmt.Println(val)
		assert.Equal(t, val, 1_009_985.00000)
	})
}