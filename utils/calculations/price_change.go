package calculations

import (
	"errors"

	"github.com/shopspring/decimal"
)

func CalculatePriceChange(open, close, volume float64) (float64, error) {
	if volume < 0 {
		return 0, errors.New("volume cannot be negative")
	}

	val, _ := decimal.NewFromFloat((close - open) * volume).Round(5).Float64()

	return val, nil
}

func CalculatePriceChangePercent(open, close float64) float64 {
	val := (close-open)/open
	rounded, _ := decimal.NewFromFloat(val).Round(5).Float64()

	return rounded
}

func CalculateRiskToReward(open, close, stopLoss float64) float64 {
	val, _ := decimal.NewFromFloat((close - open) / (open - stopLoss)).Round(5).Float64()

	return val
}

func CalculateBalanceAfter(balanceBefore, netChange, commission float64) float64 {
	balance := balanceBefore + netChange - commission
	val, _ := decimal.NewFromFloat(balance).Round(5).Float64()

	return val
}
