package utils

import (
	"app/models"

	"github.com/labstack/echo/v4"
)

type M map[string]interface{}

func JSONResponse(c echo.Context, status int, data interface{}, err ...error) error {
	msg := "success"

	if len(err) > 0 {
		msg = err[0].Error()
	}

	if status == 404 {
		msg = "Not found error"
	}

	rtn := models.BaseResponse{
		StatusCode: status,
		Message:    msg,
		Response:   data,
	}

	return c.JSON(status, rtn)
}