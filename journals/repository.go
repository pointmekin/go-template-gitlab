package journals

import (
	"fmt"

	"app/models"

	"gorm.io/gorm"
)

type repo struct {
	Db *gorm.DB
}

type JournalRepository interface {
	CreateJournal(journal models.JournalEntry) (models.JournalEntry, error)
	GetJournalById(id int) (models.JournalEntry, error)
	UpdateJournal(id int, journal models.JournalEntry) (models.JournalEntry, error)
	DeleteJournalById(id int) error
}

func (r *repo) CreateJournal(journal models.JournalEntry) (models.JournalEntry, error) {
	err := r.Db.Table("journal_entries").Create(&journal).Error
	if err != nil {
		fmt.Println(err.Error())
		return models.JournalEntry{}, err
	}

	return journal, nil
}

func (r *repo) GetJournalById(id int) (models.JournalEntry, error) {
	var journal models.JournalEntry
	err := r.Db.Table("journal_entries").First(&journal, id).Error
	if err != nil {
		return models.JournalEntry{}, err
	}

	return journal, nil
}

func (r *repo) UpdateJournal(id int, update models.JournalEntry) (models.JournalEntry, error) {
	var journal models.JournalEntry
	err := r.Db.Table("journal_entries").Where("id = ?", id).Updates(update).First(&journal).Error
	if err != nil {
		return models.JournalEntry{}, err
	}

	return journal, nil
}

func (r *repo) DeleteJournalById(id int) error {
	err := r.Db.Table("journal_entries").Delete(&models.JournalEntry{}, id).Error
	if err != nil {
		return err
	}

	return nil
}
