package journals

import (
	"net/http"
	"strconv"

	"app/models"
	"app/utils"

	"github.com/labstack/echo/v4"
)

type controller struct {
	serv Service
}

func InitController(s *service) *controller {
	return &controller{serv: s}
}

func (ctr *controller) CreateOne(c echo.Context) error {
	journal := new(models.JournalEntry)
	if err := c.Bind(journal); err != nil {
		return utils.JSONResponse(c, http.StatusBadRequest, nil, err)
	}

	if err := c.Validate(journal); err != nil {
		return utils.JSONResponse(c, http.StatusBadRequest, nil, err)
	}

	created, status, err := ctr.serv.CreateOne(*journal)
	if err != nil {
		return utils.JSONResponse(c, status, nil, err)
	}

	return utils.JSONResponse(c, http.StatusOK, created)
}

func (ctr *controller) GetOne(c echo.Context) error {
	id := c.Param("id")
	_id, err := strconv.Atoi(id)
	if err != nil {
		return utils.JSONResponse(c, http.StatusBadRequest, nil, err)
	}

	journal, status, err := ctr.serv.GetOne(_id)
	if err != nil {
		return utils.JSONResponse(c, status, nil, err)
	}

	return utils.JSONResponse(c, status, journal)
}

func (ctr *controller) UpdateOne(c echo.Context) error {
	id := c.Param("id")
	_id, err := strconv.Atoi(id)
	if err != nil {
		return utils.JSONResponse(c, http.StatusBadRequest, nil, err)
	}

	journal := new(models.JournalEntry)
	if err := c.Bind(journal); err != nil {
		return utils.JSONResponse(c, http.StatusBadRequest, nil, err)
	}

	if err := c.Validate(journal); err != nil {
		return utils.JSONResponse(c, http.StatusBadRequest, nil, err)
	}

	updated, status, err := ctr.serv.UpdateOne(_id, *journal)
	if err != nil {
		return utils.JSONResponse(c, status, nil, err)
	}

	return utils.JSONResponse(c, status, updated)
}

func (ctr *controller) DeleteOne(c echo.Context) error {
	id := c.Param("id")
	_id, err := strconv.Atoi(id)
	if err != nil {
		return utils.JSONResponse(c, http.StatusBadRequest, nil, err)
	}

	status, err := ctr.serv.DeleteOne(_id)
	if err != nil {
		return utils.JSONResponse(c, status, nil, err)
	}

	return utils.JSONResponse(c, http.StatusOK, nil)
}
