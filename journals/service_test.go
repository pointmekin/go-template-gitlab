package journals

import (
	"net/http"
	"testing"
	"time"

	"app/models"
	"app/utils/calculations"

	"github.com/stretchr/testify/assert"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var entries []models.JournalEntry

func init() {
	entries = append(entries, models.JournalEntry{
		DateEntry:       time.Now(),
		DateExit:        time.Now(),
		Symbol:          "AAPL",
		TradeType:       models.TradeLong,
		Volume:          100,
		Open:            100,
		StopLoss:        95,
		Close:           200,
		BalanceBefore:   1_000_000,
		TotalCommission: 15,
		StrategyName:    "Test",
		Note:            "Test",
		ChartUrl:        "https://www.google.com",
	})
}

func Test_CreateOne(t *testing.T) {
	mockDb, mock, err := sqlmock.New()
	if err != nil {
		t.Error("Failed to create mock database", err.Error())
	}

	defer mockDb.Close()

	dialector := postgres.New(postgres.Config{
		Conn:       mockDb,
		DriverName: "postgres",
	})
	db, _ := gorm.Open(dialector, &gorm.Config{})

	entry := entries[0]
	r2r := calculations.CalculateRiskToReward(entry.Open, entry.Close, entry.StopLoss)
	netChange, _ := calculations.CalculatePriceChange(entry.Open, entry.Close, entry.Volume)
	netChangePercent := calculations.CalculatePriceChangePercent(entry.Open, entry.Close)
	balanceAfter := calculations.CalculateBalanceAfter(entry.BalanceBefore, netChange, entry.TotalCommission)

	t.Run("Should successfully create a journal entry", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{
			"ID", "date_entry", "date_exit", "symbol", "trade_type", "volume", "open", "stop_loss", "close", "risk_to_reward_ratio", "net_change", "net_change_percent", "balance_before", "balance_after", "total_commission", "strategy_name", "note", "chart_url"}).AddRow(
			1, entry.DateEntry, entry.DateExit, entry.Symbol, entry.TradeType, entry.Volume, entry.Open, entry.StopLoss, entry.Close, r2r, netChange, netChangePercent, entry.BalanceBefore, balanceAfter, entry.TotalCommission, entry.StrategyName, entry.Note, entry.ChartUrl)

		mock.ExpectBegin()
		mock.ExpectQuery(`INSERT`).WillReturnRows(rows)
		mock.ExpectCommit()

		serv := InitService(db)
		entry, status, err := serv.CreateOne(entry)

		assert.Nil(t, err)
		assert.Equal(t, int(entry.ID), 1)
		assert.Equal(t, entry.Symbol, "AAPL")
		assert.Equal(t, entry.TradeType, models.TradeType("long"))
		assert.Equal(t, entry.Volume, float64(100))
		assert.Equal(t, entry.Open, float64(100))
		assert.Equal(t, entry.StopLoss, float64(95))
		assert.Equal(t, entry.Close, float64(200))
		assert.Equal(t, entry.BalanceBefore, float64(1_000_000))
		assert.Equal(t, entry.TotalCommission, float64(15))
		assert.Equal(t, entry.RiskToRewardRatio, r2r)
		assert.Equal(t, entry.NetChange, netChange)
		assert.Equal(t, entry.NetChangetPercent, netChangePercent)
		assert.Equal(t, entry.BalanceAfter, balanceAfter)
		assert.Equal(t, entry.StrategyName, "Test")
		assert.Equal(t, entry.Note, "Test")
		assert.Equal(t, entry.ChartUrl, "https://www.google.com")

		assert.Nil(t, err)
		assert.Equal(t, status, http.StatusCreated)
	})
}
