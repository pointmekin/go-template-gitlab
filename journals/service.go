package journals

import (
	"app/models"
	"app/utils/calculations"
	"net/http"

	"gorm.io/gorm"
)

type service struct {
	journalRepo JournalRepository
	modelName   string
}

type Service interface {
	CreateOne(journal models.JournalEntry) (models.JournalEntry, int, error)
	GetOne(id int) (models.JournalEntry,int,  error)
	UpdateOne(id int, journal models.JournalEntry) (models.JournalEntry, int, error)
	DeleteOne(id int) (int, error)
}

func InitService(db *gorm.DB) *service {
	return &service{
		journalRepo: &repo{Db: db},
		modelName:   "journal_entries",
	}
}


func (s *service) CreateOne(journal models.JournalEntry) (models.JournalEntry, int, error) {
	change, err := calculations.CalculatePriceChange(journal.Open, journal.Close, journal.Volume)

	if err != nil {
		return models.JournalEntry{}, http.StatusBadRequest, err
	}

	journal.NetChange = change
	journal.NetChangetPercent = calculations.CalculatePriceChangePercent(journal.Open, journal.Close)
	journal.BalanceAfter = calculations.CalculateBalanceAfter(journal.BalanceBefore, journal.NetChange, journal.TotalCommission)
	journal.RiskToRewardRatio = calculations.CalculateRiskToReward(journal.Open, journal.Close, journal.StopLoss)

	created, err := s.journalRepo.CreateJournal(journal)

	if err != nil {
		return models.JournalEntry{}, http.StatusBadRequest, err
	}

	return created, http.StatusCreated, err
}


func (s *service) GetOne(id int) (models.JournalEntry, int, error) {
	journal, err := s.journalRepo.GetJournalById(id)

	if err != nil {
		return models.JournalEntry{}, http.StatusNotFound, err
	}
	return journal, http.StatusOK, err
}

func (s *service) UpdateOne(id int, journal models.JournalEntry) (models.JournalEntry, int, error) {
	_, err := s.journalRepo.GetJournalById(id)

	if err != nil {
		return models.JournalEntry{}, http.StatusNotFound, err	
	}

	change, err := calculations.CalculatePriceChange(journal.Open, journal.Close, journal.Volume)
	if err != nil {
		return models.JournalEntry{}, http.StatusBadRequest, err
	}

	journal.NetChange = change
	journal.NetChangetPercent = calculations.CalculatePriceChangePercent(journal.Open, journal.Close)
	journal.BalanceAfter = calculations.CalculateBalanceAfter(journal.BalanceBefore, journal.NetChange, journal.TotalCommission)
	journal.RiskToRewardRatio = calculations.CalculateRiskToReward(journal.Open, journal.Close, journal.StopLoss)

	updated, err := s.journalRepo.UpdateJournal(id, journal)
	return updated, http.StatusAccepted, err
}


func (s *service) DeleteOne(id int) (int, error) {
	_, err := s.journalRepo.GetJournalById(id)
	
	if err != nil {
		return http.StatusNotFound, err
	}

	err = s.journalRepo.DeleteJournalById(id)
	return http.StatusAccepted, err
}