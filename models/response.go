package models

type BaseResponse struct {
	StatusCode int				 `json:"status_code"`
	Message		string			 `json:"message"`
	Response 	interface{}	 `json:"response"`
}