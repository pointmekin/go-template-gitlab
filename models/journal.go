package models

import (
	"time"

	"gorm.io/gorm"
)

type TradeType string
const (
	TradeLong TradeType = "long"
	TradeShort TradeType = "short"
)

type JournalEntry struct {
	gorm.Model
	DateEntry         time.Time `json:"date_entry" gorm:"date_entry" validate:"required"`
	DateExit          time.Time `json:"date_exit" gorm:"date_exit" validate:"required"`
	Symbol            string    `json:"symbol" gorm:"symbol" validate:"required"`
	TradeType         TradeType    `json:"trade_type" gorm:"trade_type" validate:"required"`
	Volume            float64   `json:"volume" gorm:"volume" validate:"required"`
	StopLoss					float64   `json:"stop_loss" gorm:"stop_loss" validate:"required"`
	Open              float64   `json:"open" gorm:"open" validate:"required"`
	Close             float64   `json:"close" gorm:"close" validate:"required"`
	// calculated
	RiskToRewardRatio float64   `json:"risk_to_reward_ratio" gorm:"risk_to_reward_ratio"`
	// calculated
	NetChange         float64   `json:"net_change" gorm:"net_change"` 
	// calculated
	NetChangetPercent float64   `json:"net_change_percent" gorm:"net_change_percent"`
	BalanceBefore     float64   `json:"balance_before" gorm:"balance_before" validate:"required"`
	// calculated
	BalanceAfter      float64   `json:"balance_after" gorm:"balance_after"`
	TotalCommission   float64   `json:"total_commission" gorm:"total_commission" validate:"required"`
	StrategyName      string    `json:"strategy_name" gorm:"strategy_name" validate:"required"`
	Note              string    `json:"note" gorm:"note" validate:"required"`
	ChartUrl          string    `json:"chart_url" gorm:"chart_url" validate:"required"`
}
