package handlers

import (
	"app/models"
	"fmt"
	"log"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

func InitPostgresDB(c Config) *gorm.DB {
	dsn := fmt.Sprintf(
		"host=%s user=%s password=%s dbname=%s port=%s sslmode=disable Timezone=Asia/GMT+7",
		c.Database.Host,
		c.Database.User,
		c.Database.Password,
		c.Database.Dbname,
		c.Database.Port,
	)

	config := &gorm.Config{}

	if string(c.Environment) == string(ConfigDEV) {
		config.Logger = logger.Default.LogMode(logger.Info)
	}

	db, err := gorm.Open(postgres.New(
		postgres.Config{
			DSN: dsn,
			PreferSimpleProtocol: true,
		}), config)

	if err != nil {
		log.Fatal("Error connecting to database", err.Error())
		panic(err.Error())
	}

	db.AutoMigrate(&models.JournalEntry{})

	return db
}