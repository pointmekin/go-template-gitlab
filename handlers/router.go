package handlers

import (
	"app/journals"

	"github.com/labstack/echo/v4"
)

func InitRoutes(e *echo.Echo, configs Config) {
	postgresDB := InitPostgresDB(configs)

	journalServ := journals.InitService(postgresDB)
	journalCtr := journals.InitController(journalServ)

	journalApi := e.Group("/api/journal")
	journalApi.GET("/:id", journalCtr.GetOne)
	journalApi.POST("", journalCtr.CreateOne)
	journalApi.PUT("/:id", journalCtr.UpdateOne)
	journalApi.DELETE("/:id", journalCtr.DeleteOne)
}
