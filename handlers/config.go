package handlers

import (
	"log"
	"path/filepath"

	"github.com/spf13/viper"
)

type config string
const (
	ConfigDEV config = "dev"
	ConfigPROD config = "prod"
)

type Config struct {
	Environment config
	Database struct {
		Host string
		User string
		Password string
		Dbname string
		Port string
	}
}

func(c *Config) Init(configType string) *Config {
	v := viper.New()
	v.AddConfigPath((filepath.Join(".", "configs")))

	var configName string
	if configType == string(ConfigPROD) {
		configName = "config.prod"
	}else{
		configName = "config.dev"
	}

	v.SetConfigName(configName)

	if err := v.ReadInConfig(); err != nil {
		log.Fatal("Cannot read config", err.Error())
		return &Config{}
	}

	if err := v.Unmarshal(&c); err != nil {
		log.Fatal("Cannot unmarshal config", err.Error())
		return &Config{}
	}

	log.Println("Initializing Config using:", configName)
	return c
}